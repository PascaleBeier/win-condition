<?php

use Illuminate\Database\Seeder;

class ChampionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $endpoint = \App\Models\Endpoint::find('EUW');
        $client = new \App\Api\RiotGames\ApiClient($endpoint);
        $requestMethod = new \App\Api\RiotGames\Methods\Champions();
        $client->request($requestMethod);
    }
}
