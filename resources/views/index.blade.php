<!doctype html>
<html>
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ asset("css/app.css") }}">
</head>

<body>
<div id="app">
    <create-wincondition></create-wincondition>
</div>
<script src="{{ asset("js/app.js") }}"></script>
</body>
</html>
