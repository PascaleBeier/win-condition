<?php

namespace App\Repository;

use App\Models\Endpoint;

final class EndpointRepository extends InMemoryRepository
{
    public function findByRegion(string $region): ?Endpoint
    {
        return $this->find('region', $region);
    }
}
