<?php

namespace App\Repository;

use App\Api\RiotGames\ApiClient;
use App\Models\Endpoint;

/**
 * The ApiClientRepository holds a Collection of Api Clients,
 * each of which contain a configured Guzzle Client
 * Instance for every available regional Endpoint.
 *
 * The Clients are accessible via Hostname, e.g. $this->items['euw1']
 * holds the configured Guzzle Client for EUW.
 *
 * As with the EndpointRepository, this Repository is populated in a Service Provider.
 *
 * On Application Level, this Repository should be used in Conjunction
 * with an Endpoint Object to ensure validity and catch errors early.
 *
 * @example
 * $endpoint = $this->endpointRepository->findByRegion($request->region);
 * $apiClient = $this->apiClientRepository->getForEndpoint($endpoint)
 */
final class ApiClientRepository extends InMemoryRepository
{
    /**
     * Retrieve a configured Api Client Instance for the given regional Endpoint.
     *
     * @param Endpoint $endpoint
     *
     * @return ApiClient|null
     */
    public function getForEndpoint(Endpoint $endpoint): ?ApiClient
    {
        return $this->offsetGet($endpoint->getHost());
    }
}
