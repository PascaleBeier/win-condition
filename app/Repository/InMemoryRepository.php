<?php

namespace App\Repository;

use App\Models\InMemoryModel;

/**
 * Repository Object for Management of InMemory Objects.
 * To make these generally available, these should
 * usually be populated in Service Providers.
 */
abstract class InMemoryRepository implements \Countable, \IteratorAggregate, \ArrayAccess
{
    /** @var array Repository Items */
    private $items = [];

    /**
     * Retrieve all Items of the Repository.
     *
     * @return array
     */
    public function all(): array
    {
        return $this->items;
    }

    /**
     * Add an Item to the Repository.
     *
     * @param InMemoryModel $model
     */
    public function add(InMemoryModel $model): void
    {
        $this->items[] = $model;
    }

    /**
     * Find an Item of the Repository by a key => value pair.
     *
     * @param string $key
     * @param $value
     *
     * @return InMemoryModel|null
     */
    public function find(string $key, $value): ?InMemoryModel
    {
        return array_first($this->items, function (InMemoryModel $item) use ($key, $value) {
            $getter = 'get'.ucfirst($key);

            return $item->{$getter}() === $value;
        });
    }

    /**
     * ArrayAccess.
     *
     * @param mixed $offset
     * @param mixed $value
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->items[] = $value;
        } else {
            $this->items[$offset] = $value;
        }
    }

    /**
     * ArrayAccess.
     *
     * @param mixed $offset
     *
     * @return bool
     */
    public function offsetExists($offset)
    {
        return isset($this->items[$offset]);
    }

    /**
     * ArrayAccess.
     *
     * @param mixed $offset
     */
    public function offsetUnset($offset)
    {
        unset($this->items[$offset]);
    }

    /**
     * ArrayAccess.
     *
     * @param mixed $offset
     *
     * @return mixed|null
     */
    public function offsetGet($offset)
    {
        return isset($this->items[$offset]) ? $this->items[$offset] : null;
    }

    /**
     * Countable implementation.
     *
     * @return int
     */
    public function count()
    {
        return count($this->items);
    }

    /**
     * ArrayIterator implementation.
     *
     * @return \ArrayIterator
     */
    public function getIterator(): \ArrayIterator
    {
        return new \ArrayIterator($this->items);
    }
}
