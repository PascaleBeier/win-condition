<?php

namespace App\Contracts;

interface RequestMethodContract
{
    public function getRequest(): string;
}
