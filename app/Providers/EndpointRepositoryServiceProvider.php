<?php

namespace App\Providers;

use App\Models\Endpoint;
use App\Repository\EndpointRepository;
use Illuminate\Support\ServiceProvider;

class EndpointRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(EndpointRepository::class, function ($app) {
            $endpointRepository = new EndpointRepository();

            $regions = [
                ['BR', 'br1'],
                ['EUNE', 'eun1'],
                ['EUW', 'euw1'],
                ['JP', 'jp1'],
                ['KR', 'kr'],
                ['LAN', 'la1'],
                ['LAS', 'la2'],
                ['NA', 'na1'],
                ['OCE', 'oc1'],
                ['TR', 'tr1'],
                ['RU', 'ru'],
                ['PBE', 'pbe1'],
            ];

            foreach ($regions as $region) {
                $endpoint = new Endpoint();
                $endpoint
                    ->setRegion($region[0])
                    ->setHost($region[1]);

                $endpointRepository->add($endpoint);
            }

            return $endpointRepository;
        });
    }
}
