<?php

namespace App\Providers;

use App\Api\RiotGames\ApiClient;
use App\Models\Endpoint;
use App\Repository\ApiClientRepository;
use App\Repository\EndpointRepository;
use GuzzleHttp\Client;
use Illuminate\Config\Repository;
use Illuminate\Foundation\Application;
use Illuminate\Support\ServiceProvider;
use Psr\Log\LoggerInterface;

/**
 * The point of this Service Provider is that we can create
 * all needed Api Clients for all regional Endpoints at once.
 */
class ApiClientRepositoryProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(ApiClientRepository::class, function (Application $app) {
            // Prepare the Repository Object
            $apiClientRepository = new ApiClientRepository();
            $config = new Repository(config('riot'));
            $redis = $app->make('redis');
            $logger = $app->make(LoggerInterface::class);

            // Create a new Guzzle Client and Api Client for each available Regional Endpoint
            foreach ($app->make(EndpointRepository::class) as $endpoint) {
                /** @var Endpoint $endpoint */
                $base_uri = str_replace('{host}', $endpoint->getHost(), $config->get('uri'));

                $client = new Client([
                    'base_uri' => $base_uri,
                    'headers'  => ['X-Riot-Token' => $config->get('key')],
                ]);

                $apiClient = new ApiClient($config, $redis, $logger);
                // Add the Guzzle Client Instance to the Api Client Object
                $apiClient->setClient($client);
                // Populate the Repository Items like "euw1" => ApiClient::class
                $apiClientRepository->offsetSet($endpoint->getHost(), $apiClient);
            }

            return $apiClientRepository;
        });
    }
}
