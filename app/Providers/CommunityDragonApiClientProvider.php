<?php

namespace App\Providers;

use App\Api\CommunityDragon\ApiClient;
use GuzzleHttp\Client;
use Illuminate\Config\Repository;
use Illuminate\Support\ServiceProvider;

class CommunityDragonApiClientProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(ApiClient::class, function ($app) {
            $config = new Repository(config('communitydragon'));
            $client = new Client(['base_uri' => $config->get('uri')]);
            $cache = $app->make(\Illuminate\Cache\Repository::class);

            return new ApiClient($client, $config, $cache);
        });
    }
}
