<?php

namespace App\Api\RiotGames\Support;

/**
 * Riot Games API Response Headers.
 */
final class Header
{
    // The application rate limits currently being applied to your API key.
    const APP_RATE_LIMIT = 'X-App-Rate-Limit';
    // The number of calls to a specific method that have been made during a specific rate limit.
    const APP_RATE_LIMIT_COUNT = 'X-App-Rate-Limit-Count';
    // The method rate limits currently being applied to your API key.
    const METHOD_RATE_LIMIT = 'X-Method-Rate-Limit';
    // The number of calls to a specific method that have been made during a specific rate limit.
    const METHOD_RATE_LIMIT_COUNT = 'X-Method-Rate-Limit-Count';
    // The remaining number of seconds before the rate limit resets. Applies to both user and service rate limits.
    const RETRY_AFTER = 'Retry-After';
}
