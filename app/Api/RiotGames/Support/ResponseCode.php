<?php

namespace App\Api\RiotGames\Support;

/**
 * Riot Games API Response Code Handling.
 */
final class ResponseCode
{
    // The only way to really expect a valid JSON Response Body.
    const SUCCESS = 200;
    // Usually when the API Key is incorrect or missing.
    const FORBIDDEN = 401;
    // Specific API Key Blacklisted.
    const BLACKLISTED = 403;
    // Rate Limit Exceeded.
    const RATE_LIMIT_EXCEEDED = 429;
}
