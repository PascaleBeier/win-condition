<?php

namespace App\Api\RiotGames\Methods;

use App\Api\RiotGames\Support\Endpoint;
use Illuminate\Support\Collection;

final class Summoner extends AbstractMethod
{
    public $endpoint = Endpoint::SUMMONER__SUMMONERS_BY_NAME;

    /** @var string Summoner Name */
    private $name;

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * Fill Request with given summoner name.
     *
     * @return string
     */
    public function getRequest(): string
    {
        return str_replace('{summonerName}', $this->name, $this->endpoint);
    }

    /**
     * Cache Summoner Information for two weeks.
     *
     * @return int
     */
    public function getCacheDuration(): int
    {
        return 60 * 24 * 14;
    }

    /**
     * @return Collection
     */
    public function getFromDatabase(): Collection
    {
        return \App\Models\Summoner::where('name', $this->name)->first()->toCollection();
    }

    /**
     * @param array $responseArray
     *
     * @return Collection
     */
    public function createModelCollection(array $responseArray): Collection
    {
        $model = new \App\Models\Summoner();

        return $model->fill($responseArray)->toCollection();
    }
}
