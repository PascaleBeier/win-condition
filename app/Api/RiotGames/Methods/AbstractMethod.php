<?php

namespace App\Api\RiotGames\Methods;

use App\Contracts\RequestMethodContract;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;

/**
 * Class AbstractMethod.
 */
abstract class AbstractMethod implements RequestMethodContract
{
    /**
     * Pass additional Request Options to the Guzzle Client.
     *
     * @var array
     */
    public $requestOptions = ['http_errors' => false];

    /**
     * Set to true, if the Model is
     * persisted to the Database.
     *
     * @var bool
     */
    public $hasDatabaseModel = true;

    /**
     * Set to true, if the Model is
     * persisted to the Cache.
     *
     * @var bool
     */
    public $hasCache = true;

    /**
     * The Endpoint Request Path
     * of the Riot API.
     *
     * @var string
     */
    public $endpoint;

    /**
     * Override this method if the Paths needs
     * further manipulation, e.g. replacement
     * of placeholders.
     *
     * @return string
     */
    public function getRequest(): string
    {
        return $this->endpoint;
    }

    /**
     * Allows customization of the cache id
     * per Request Method.
     *
     * @return string
     */
    public function getCacheKey(): string
    {
        return str_slug($this->getRequest());
    }

    /**
     * Search Query in the Database.
     *
     * @example \App\Models\Summoner::where("name", $this->name);
     *
     * @return \Illuminate\Database\Eloquent\Collection|null
     */
    public function getFromDatabase(): ?Collection
    {
        return null;
    }

    /**
     * Return the Desired Cache Duration in Minutes.
     *
     * @return int
     */
    public function getCacheDuration(): int
    {
        return config('riot.api.cache');
    }

    /**
     * Retrieve a Request Method from Database or Cache.
     * Returns false or the aggregated object.
     *
     * @return Collection|null
     */
    final public function retrieveFromDatabaseOrCache(): ?Collection
    {
        // Return cached Response if available
        if ($this->hasCache) {
            return Cache::get($this->getCacheKey());
        }

        // Get a collection from the Database where applicable.
        if ($this->hasDatabaseModel) {
            return $this->getFromDatabase();
        }

        // Otherwise, return null
        return null;
    }

    /**
     * Update a Collection in the Database or Cache.
     *
     * @param Collection $collection
     */
    final public function updateDatabaseOrCache(Collection $collection): void
    {
        if ($this->hasDatabaseModel) {
            $collection->each(function (Model $model) {
                /* @var Model|Builder $model */
                $model->updateOrCreate($model->getAttributes());
            });
        }

        if ($this->hasCache) {
            Cache::put($this->getCacheKey(), $collection, $this->getCacheDuration());
        }
    }

    /**
     * Transform the Response Object to a Collection.
     *
     * @param array $responseArray
     *
     * @return Collection
     */
    abstract public function createModelCollection(array $responseArray): Collection;
}
