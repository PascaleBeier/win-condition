<?php

namespace App\Api\RiotGames\Methods;

use App\Api\RiotGames\Support\Endpoint;
use App\Models\Champion;
use Illuminate\Support\Collection;

final class Champions extends AbstractMethod
{
    public $endpoint = Endpoint::LOL_STATIC_DATA__CHAMPIONS;

    public function getFromDatabase(): Collection
    {
        return Champion::all();
    }

    /**
     * @param array $responseArray
     *
     * @return Collection
     */
    public function createModelCollection(array $responseArray): Collection
    {
        $collection = new Collection();

        foreach ($responseArray['data'] as $champion) {
            $model = new Champion();
            $model->fill($champion);
            $collection->push($model);
        }

        return $collection;
    }
}
