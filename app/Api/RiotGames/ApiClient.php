<?php

namespace App\Api\RiotGames;

use App\Api\RiotGames\Methods\AbstractMethod;
use App\Api\RiotGames\Support\ResponseCode;
use Illuminate\Config\Repository as Config;
use Illuminate\Redis\RedisManager;
use Illuminate\Support\Collection;
use Psr\Log\LoggerInterface;

/**
 * Class ApiClient.
 */
final class ApiClient
{
    /** @var \GuzzleHttp\Client */
    private $client;

    /** @var RedisManager */
    private $redis;

    /** @var Config */
    private $config;

    /** @var LoggerInterface */
    private $logger;

    public function __construct(Config $config, RedisManager $redis, LoggerInterface $logger)
    {
        $this->config = $config;
        $this->redis = $redis;
        $this->logger = $logger;
    }

    /**
     * @param \GuzzleHttp\Client $client
     */
    public function setClient(\GuzzleHttp\Client $client): void
    {
        $this->client = $client;
    }

    /**
     * @param AbstractMethod $requestMethod
     *
     * @throws \Illuminate\Contracts\Redis\LimiterTimeoutException
     *
     * @return Collection|null
     */
    final public function request(AbstractMethod $requestMethod): ?Collection
    {
        // Try getting the object from Cache or Database first.
        $collection = $requestMethod->retrieveFromDatabaseOrCache();

        // Otherwise, execute the API Request.
        if ($collection === null) {
            $collection = $this->makeRequest($requestMethod);
        }

        // If the previous Response was not null and the result set
        // is not empty, we persist the result set to the database.
        if ($collection !== null && $collection->isNotEmpty()) {
            $requestMethod->updateDatabaseOrCache($collection);
        }

        return $collection;
    }

    /**
     * @param AbstractMethod $method
     *
     * @throws \Illuminate\Contracts\Redis\LimiterTimeoutException
     *
     * @return Collection|null
     */
    final private function makeRequest(AbstractMethod &$method): ?Collection
    {
        $output = null;

        $pairs = explode(',', $this->config->get('limits'));
        $allowEveryFirst = explode(':', $pairs[0]);
        $allowEverySecond = explode(':', $pairs[1]);

        $this->redis->throttle('app-limits')
        ->allow((int) $allowEveryFirst[0])
        ->every((int) $allowEveryFirst[1])
        ->allow((int) $allowEverySecond[0])
        ->every((int) $allowEverySecond[1])
        ->then(function () use (&$method, &$output) {
            $response = $this->client->get($method->getRequest(), $method->requestOptions);

            switch ($response->getStatusCode()) {
                case ResponseCode::SUCCESS:
                    // The Only way we can ever expect a valid JSON Response Body.
                    $responseObject = json_decode($response->getBody()->getContents(), true);
                    $output = $method->createModelCollection($responseObject);
                    break;
                case ResponseCode::BLACKLISTED:
                    $this->logger->warning("Application Blacklisted for Endpoint $method->endpoint");
                    break;
                case ResponseCode::FORBIDDEN:
                    $this->logger->warning("Forbidden Response for Endpoint $method->endpoint");
                    break;
                case ResponseCode::RATE_LIMIT_EXCEEDED:
                    $this->logger->warning("Rate Limit exceeded for Endpoint $method->endpoint");
                    break;
                default:
                    break;
            }
        }, function () {
            $this->logger->notice('Failed to acquire key, Internal App Rate Limit Reached');
        });

        return $output;
    }
}
