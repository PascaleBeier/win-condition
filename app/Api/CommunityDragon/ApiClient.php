<?php

namespace App\Api\CommunityDragon;

use App\Api\CommunityDragon\Methods\AbstractMethod;
use App\Api\CommunityDragon\Support\Resource;
use App\Api\CommunityDragon\Support\ResponseCode;
use GuzzleHttp\Client;
use Illuminate\Contracts\Cache\Repository as Cache;
use Illuminate\Contracts\Config\Repository as Config;

/**
 * Client for Community Dragon API Interaction.
 */
class ApiClient
{
    /** @var Client Guzzle HTTP Client */
    private $client;

    /** @var Config Configuration */
    private $config;

    /** @var string Current Request Path */
    private $path;

    /** @var AbstractMethod Current Request Method */
    private $method;

    /** @var Cache */
    private $cache;

    public function __construct(Client $client, Config $config, Cache $cache)
    {
        $this->client = $client;
        $this->config = $config;
        $this->cache = $cache;
    }

    public function request(AbstractMethod $method)
    {
        $this->method = $method;

        $this->path = $this->config->get('uri').'/'.
                      $this->config->get('version').
                      $this->method->getRequest();

        if ($this->method->resource === Resource::MEDIA) {
            return $this->handleMedia();
        }

        // JSON Resources need to be fetched with
        // the Guzzle Client and encoded.
        if ($this->method->resource === Resource::JSON) {
            return $this->handleJSON();
        }
    }

    private function handleJSON(): ?array
    {
        $result = null;

        $hasCache = $this->config->get('json.cache.enabled');
        $cacheKey = str_slug($this->path);

        // Before we make any Requests, consult the Caches.
        if ($hasCache) {
            // The Cache Identifier is built with the provided Path.
            if ($this->cache->has($cacheKey)) {
                dd($this->cache->get($cacheKey));

                return $this->cache->get($cacheKey);
            }
        }
        // Make a Request to the API
        $response = $this->client->get($this->path, $this->config->get('json.client.options'));
        // We only expect a valid JSON Response Body with a Success Header.
        if ($response->getStatusCode() === ResponseCode::SUCCESS) {
            // JSON Decoded Response Body according to configuration.
            $result = json_decode($response->getBody()->getContents(), $this->config->get('json.toArray'));
            // Store the Result in the Cache if wanted.
            if ($hasCache) {
                $this->cache->put($cacheKey, $result, $this->config->get('json.cache.duration'));
            }
        }

        return $result;
    }

    public function handleMedia(): string
    {
        return $this->path;
    }
}
