<?php

namespace App\Api\CommunityDragon\Methods;

use App\Api\CommunityDragon\Support\Endpoint;
use App\Api\CommunityDragon\Support\Resource;

class ChampionDataById extends AbstractMethod
{
    public $endpoint = Endpoint::CHAMPION__DATA_BY_ID;

    public $resource = Resource::JSON;

    /** @var int key */
    private $key;

    public function __construct(int $key)
    {
        $this->key = $key;
    }

    public function getRequest(): string
    {
        return str_replace('{championId}', $this->key, $this->endpoint);
    }
}
