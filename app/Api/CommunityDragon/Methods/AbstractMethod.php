<?php

namespace App\Api\CommunityDragon\Methods;

use App\Api\CommunityDragon\Support\Resource;
use App\Contracts\RequestMethodContract;

abstract class AbstractMethod implements RequestMethodContract
{
    public $resource = Resource::MEDIA;

    public $endpoint;

    abstract public function getRequest(): string;
}
