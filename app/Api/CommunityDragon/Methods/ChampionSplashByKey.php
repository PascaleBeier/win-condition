<?php

namespace App\Api\CommunityDragon\Methods;

use App\Api\CommunityDragon\Support\Endpoint;

class ChampionSplashByKey extends AbstractMethod
{
    public $endpoint = Endpoint::CHAMPION__SPLASH_BY_KEY;

    /** @var string key */
    private $key;

    public function __construct(string $key)
    {
        $this->key = $key;
    }

    public function getRequest(): string
    {
        return str_replace('{championKey}', $this->key, $this->endpoint);
    }
}
