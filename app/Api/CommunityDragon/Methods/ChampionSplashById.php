<?php

namespace App\Api\CommunityDragon\Methods;

use App\Api\CommunityDragon\Support\Endpoint;

class ChampionSplashById extends AbstractMethod
{
    public $endpoint = Endpoint::CHAMPION__SPLASH_BY_ID;

    /** @var int key */
    private $key;

    public function __construct(int $key)
    {
        $this->key = $key;
    }

    public function getRequest(): string
    {
        return str_replace('{championId}', $this->key, $this->endpoint);
    }
}
