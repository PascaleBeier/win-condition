<?php

namespace App\Api\CommunityDragon\Support;

/**
 * Community Dragon Api Endpoints.
 */
final class Endpoint
{
    const CHAMPION__SQUARE_BY_ID = '/champion/{championId}/square';
    const CHAMPION__SQUARE_BY_KEY = '/champion/{championKey}/square';

    const CHAMPION__DATA_BY_ID = '/champion/{championId}/data';
    const CHAMPION__DATA_BY_KEY = '/champion/{championKey}/data';

    const CHAMPION__SPLASH_BY_ID = '/champion/{championId}/splash-art';
    const CHAMPION__SPLASH_BY_KEY = '/champion/{championKey}/splash-art';

    const CHAMPION__SPLASH_BY_ID_BY_SKIN_ID = '/champion/{championId}/splash-art/skin/{skinId}';
    const CHAMPION__SPLASH_BY_KEY_BY_SKIN_ID = '/champion/{championKey}/splash-art/skin/{skinId}';

    const CHAMPION__SPLASH_CENTERED_BY_ID = '/champion/{championId}/splash-art/centered';
    const CHAMPION__SPLASH_CENTERED_BY_KEY = '/champion/{championKey}/splash-art/centered';

    const CHAMPION__SPLASH_CENTERED_BY_ID_BY_SKIN_ID = '/champion/{championId}/splash-art/centered/skin/{skinId}';
    const CHAMPION__SPLASH_CENTERED_BY_KEY_BY_SKIN_ID = '/champion/{championKey}/splash-art/centered/skin/{skinId}';

    const CHAMPION__TILE_BY_ID = '/champion/{championId}/tile';
    const CHAMPION__TILE_BY_KEY = '/champion/{championKey}/tile';

    const CHAMPION__TILE_BY_ID_BY_SKIN_ID = '/champion/{championId}/tile/skin/{skinId}';
    const CHAMPION__TILE_BY_KEY_BY_SKIN_ID = '/champion/{championKey}/tile/skin/{skinId}';

    const CHAMPION__PORTRAIT_BY_ID = '/champion/{championId}/portrait';
    const CHAMPION__PORTRAIT_BY_KEY = '/champion/{championKey}/portrait';

    const CHAMPION__PORTRAIT_BY_ID_BY_SKIN_ID = '/champion/{championId}/portrait/skin/{skinId}';
    const CHAMPION__PORTRAIT_BY_KEY_BY_SKIN_ID = '/champion/{championKey}/portrait/skin/{skinId}';

    const HONOR__BY_ID = '/honor/{honorId}';
    const HONOR__LEVEL_BY_ID = '/honor/{honorId}/level/{levelId}';

    const PROFILE__ICON_BY_ID = '/profile-icon/{iconId}';

    const WARD__BY_ID = '/ward/{wardId}';
    const WARD__SHADOW_BY_ID = '/ward/{wardId}/shadow';
}
