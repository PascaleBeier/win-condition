<?php

namespace App\Api\CommunityDragon\Support;

/**
 * Community Dragon Response Codes.
 */
final class ResponseCode
{
    const SUCCESS = 200;
}
