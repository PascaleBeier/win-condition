<?php

namespace App\Api\CommunityDragon\Support;

/**
 * Community Dragon Resource Types.
 *
 * With these Resource Types, the Api Client can decide
 * on how to process a response.
 *
 * For example CDN media does not need to be processed,
 * JSON however does.
 */
final class Resource
{
    const JSON = 0;
    const MEDIA = 1;
}
