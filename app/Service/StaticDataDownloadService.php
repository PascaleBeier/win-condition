<?php

namespace App\Service;

use App\Api\RiotGames\ApiClient;
use App\Api\RiotGames\Methods\TarballLinks;
use App\Contracts\ServiceContract;
use App\Models\Endpoint;
use GuzzleHttp\Client;

/**
 * Class StaticDataDownloadService.
 */
class StaticDataDownloadService implements ServiceContract
{
    public function execute(): bool
    {
        $endpoint = Endpoint::find('EUW');
        $client = new ApiClient($endpoint);
        $tarballUrl = $client->request(TarballLinks::class);
        $filename = basename($tarballUrl);

        $client = new Client(['sink' => resource_path('assets/vendor/'.$filename)]);
        $client->get($tarballUrl);

        return true;
    }
}
