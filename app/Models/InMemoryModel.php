<?php

namespace App\Models;

/**
 * Base Class for Models not being
 * persisted to the Database.
 */
abstract class InMemoryModel
{
}
