<?php

namespace App\Models;

/**
 * Regional Endpoints of the Riot Games Api.
 * Since these are needed at any time,
 * we store them in Memory.
 */
final class Endpoint extends InMemoryModel
{
    /** @var string Human Readable Region Identifier, e.g. "EUW" */
    private $region;

    /** @var string Internal Host Identifier for URI assembly, e.g. "euw1" */
    private $host;

    /**
     * @param string $host
     *
     * @return Endpoint
     */
    public function setHost(string $host): self
    {
        $this->host = $host;

        return $this;
    }

    /**
     * @param string $region
     *
     * @return Endpoint
     */
    public function setRegion(string $region): self
    {
        $this->region = $region;

        return $this;
    }

    /**
     * @return string
     */
    public function getRegion(): string
    {
        return $this->region;
    }

    /**
     * @return string
     */
    public function getHost(): string
    {
        return $this->host;
    }
}
