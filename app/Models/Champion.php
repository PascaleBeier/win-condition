<?php

namespace App\Models;

use App\Traits\Collectionable;
use Illuminate\Database\Eloquent\Model;

final class Champion extends Model
{
    use Collectionable;

    protected $fillable = ['id', 'key', 'name', 'title'];
}
