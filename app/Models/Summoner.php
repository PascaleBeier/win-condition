<?php

namespace App\Models;

use App\Traits\Collectionable;
use Illuminate\Database\Eloquent\Model;

final class Summoner extends Model
{
    use Collectionable;

    protected $fillable = ['id', 'name', 'profileIconId', 'summonerLevel', 'accountId', 'revisionDate'];
}
