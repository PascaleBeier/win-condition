<?php

namespace App\Http\Controllers;

use App\Api\RiotGames\Methods\Summoner;
use App\Repository\ApiClientRepository;
use App\Repository\EndpointRepository;
use Illuminate\Http\Request;

class WinConditionController extends Controller
{
    /** @var ApiClientRepository */
    private $apiClient;

    /** @var EndpointRepository */
    private $endpoint;

    public function __construct(ApiClientRepository $apiClient, EndpointRepository $endpoint)
    {
        $this->apiClient = $apiClient;
        $this->endpoint = $endpoint;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $client = $this->apiClient->getForEndpoint($this->endpoint->findByRegion('EUW'));

        $client->request(new Summoner('DS Examo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
