<?php

namespace App\Traits;

use Illuminate\Support\Collection;

trait Collectionable
{
    public function toCollection(): Collection
    {
        $collection = new Collection();

        if (!$this instanceof Collection) {
            $collection->push($this);
        }

        return $collection;
    }
}
