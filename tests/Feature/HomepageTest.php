<?php

namespace Tests\Feature;

use App\Repository\EndpointRepository;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class HomepageTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testHomepageFunctionality()
    {
        $response = $this->get('/');
        $response->assertSuccessful();

        // Expect to See a Select Box of all Available Endpoints.
        $endpoints = app()->make(EndpointRepository::class);
        foreach ($endpoints as $endpoint) {
            $response->assertSee($endpoint->getRegion());
        }
    }
}
