<?php

namespace Tests\Unit\Repository;

use App\Models\Endpoint;
use App\Repository\EndpointRepository;
use Tests\TestCase;

/**
 * Test Bootstrapping and Aggregation of the Endpoint Repository.
 */
class EndpointRepositoryTest extends TestCase
{
    const TOTAL_REGIONAL_ENDPOINTS = 12;

    /** @var EndpointRepository */
    private $endpointRepository;

    public function setUp()
    {
        parent::setUp();
        $this->endpointRepository = app()->make(EndpointRepository::class);
    }

    public function testCanBootstrapEndpoints()
    {
        $this->assertEquals(self::TOTAL_REGIONAL_ENDPOINTS, count($this->endpointRepository));
        foreach ($this->endpointRepository as $endpoint) {
            $this->assertInstanceOf(Endpoint::class, $endpoint);
        }
    }

    public function testFindByRegion()
    {
        /** @var Endpoint $endpointEuw */
        $endpointEuw = $this->endpointRepository->findByRegion('EUW');
        $this->assertInstanceOf(Endpoint::class, $endpointEuw);
        $this->assertEquals($endpointEuw, $this->endpointRepository->find('region', 'EUW'));
        $this->assertEquals($endpointEuw, $this->endpointRepository->find('host', 'euw1'));
        $this->assertSame(
            $this->endpointRepository->find('host', 'euw1'),
            $this->endpointRepository->find('region', 'EUW')
        );
        $this->assertEquals('euw1', $endpointEuw->getHost());
        $this->assertEquals('EUW', $endpointEuw->getRegion());
    }

    public function testArrayAccess()
    {
        $arrayIterator = 0;
        foreach ($this->endpointRepository as $endpoint) {
            $arrayIterator++;
        }

        $this->assertEquals(self::TOTAL_REGIONAL_ENDPOINTS, $arrayIterator);
    }
}
