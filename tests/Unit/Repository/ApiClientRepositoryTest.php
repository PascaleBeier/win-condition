<?php

namespace Tests\Unit\Repository;

use App\Api\RiotGames\ApiClient;
use App\Repository\ApiClientRepository;
use App\Repository\EndpointRepository;
use Tests\TestCase;

/**
 * Test Bootstrapping and Aggregation of the Api Client Repository.
 */
class ApiClientRepositoryTest extends TestCase
{
    /** @var ApiClientRepository */
    private $apiClientRepository;

    /** @var EndpointRepository */
    private $endpointRepository;

    public function setUp()
    {
        parent::setUp();
        $this->apiClientRepository = app()->make(ApiClientRepository::class);
        $this->endpointRepository = app()->make(EndpointRepository::class);
    }

    public function testCanBootstrapApiClients()
    {
        $this->assertEquals(EndpointRepositoryTest::TOTAL_REGIONAL_ENDPOINTS, count($this->apiClientRepository));
        foreach ($this->apiClientRepository as $apiClient) {
            $this->assertInstanceOf(ApiClient::class, $apiClient);
        }
    }

    public function testGetForEndpoint()
    {
        /** @var ApiClient $apiClient */
        $apiClient = $this->apiClientRepository->getForEndpoint($this->endpointRepository->findByRegion('EUW'));
        $this->assertInstanceOf(ApiClient::class, $apiClient);
    }
}
