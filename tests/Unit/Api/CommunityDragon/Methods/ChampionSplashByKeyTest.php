<?php

namespace Tests\Unit\Api\CommunityDragon\Methods;

use App\Api\CommunityDragon\Methods\AbstractMethod;
use App\Api\CommunityDragon\Methods\ChampionSplashByKey;
use Tests\Unit\Api\CommunityDragon\ApiTest;

class ChampionSplashByKeyTest extends ApiTest
{
    public $validRequest = 'Ahri';

    public $expectedPath = '/champion/Ahri/splash-art';

    public function getMethod(): AbstractMethod
    {
        $method = new ChampionSplashByKey($this->validRequest);

        return $method;
    }
}
