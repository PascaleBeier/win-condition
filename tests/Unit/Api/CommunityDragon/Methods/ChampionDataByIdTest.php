<?php

namespace Tests\Unit\Api\CommunityDragon\Methods;

use App\Api\CommunityDragon\Methods\ChampionDataById;
use Tests\Unit\Api\CommunityDragon\ApiTest;

class ChampionDataByIdTest extends ApiTest
{
    public $validRequest = '1';

    public $expectedPath = '/champion/1/data';

    public function getMethod(): \App\Api\CommunityDragon\Methods\AbstractMethod
    {
        $method = new ChampionDataById($this->validRequest);

        return $method;
    }
}
