<?php

namespace Tests\Unit\Api\CommunityDragon\Methods;

use App\Api\CommunityDragon\Methods\AbstractMethod;
use App\Api\CommunityDragon\Methods\ChampionSplashByKey;
use Tests\Unit\Api\CommunityDragon\ApiTest;

class ChampionSplashByIdTest extends ApiTest
{
    public $validRequest = 1;

    public $expectedPath = '/champion/1/splash-art';

    public function getMethod(): AbstractMethod
    {
        $method = new ChampionSplashByKey($this->validRequest);

        return $method;
    }
}
