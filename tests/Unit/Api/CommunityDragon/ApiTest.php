<?php

namespace Tests\Unit\Api\CommunityDragon;

use App\Api\CommunityDragon\ApiClient;
use App\Api\CommunityDragon\Methods\AbstractMethod;
use App\Api\CommunityDragon\Support\Resource;
use Illuminate\Support\Facades\Cache;
use Tests\TestCase;

abstract class ApiTest extends TestCase
{
    /** @var string */
    public $expectedPath;

    /** @var ApiClient */
    protected $apiClient;

    /** @var string */
    protected $prefix;

    protected function setUp()
    {
        parent::setUp();
        $this->apiClient = app()->make(ApiClient::class);
        $this->prefix = config('communitydragon.uri').'/'.config('riot.version');
    }

    abstract public function getMethod(): AbstractMethod;

    public function testRequest(): void
    {
        $method = $this->getMethod();

        if ($method->resource === Resource::MEDIA) {
            $actual = $this->apiClient->request($method);
            $expected = $this->prefix.$this->expectedPath;

            $this->assertEquals($expected, $actual);
        }

        if ($method->resource === Resource::JSON) {
            $result = $this->apiClient->request($method);

            // Expect JSON Array or Object Result.
            if (config('communitydragon.json.toArray')) {
                $this->assertInternalType('array', $result);
            } else {
                $this->assertInstanceOf(\stdClass::class, $result);
            }

            $cacheKey = str_slug(config('communitydragon.uri').'/'.
                                 config('communitydragon.version').
                                 $method->getRequest());

            $this->assertTrue(Cache::has($cacheKey));

            if (config('communitydragon.json.toArray')) {
                $this->assertInternalType('array', Cache::get($cacheKey));
            } else {
                $this->assertInstanceOf(\stdClass::class, Cache::get($cacheKey));
            }
        }
    }
}
