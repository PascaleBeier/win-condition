<?php

namespace Tests\Unit\Api\RiotGames;

use App\Api\RiotGames\ApiClient;
use App\Repository\ApiClientRepository;
use App\Repository\EndpointRepository;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

abstract class ApiTest extends TestCase
{
    use DatabaseMigrations;
    use WithFaker;

    /** @var ApiClientRepository */
    protected $apiClientRepository;

    /** @var EndpointRepository */
    protected $endpointsRepository;

    /** @var ApiClient */
    protected $apiClient;

    public function setUp()
    {
        parent::setUp();
        $this->apiClientRepository = app()->make(ApiClientRepository::class);
        $this->endpointsRepository = app()->make(EndpointRepository::class);
        $this->apiClient = $this->apiClientRepository->getForEndpoint(
            $this->endpointsRepository->findByRegion('EUW')
        );
    }

    abstract public function testValidRequest(): void;

    abstract public function testInvalidRequest(): void;
}
