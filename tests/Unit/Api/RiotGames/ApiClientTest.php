<?php

namespace Tests\Unit\Api\RiotGames;

use App\Api\RiotGames\Methods\Summoner;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;

class ApiClientTest extends ApiTest
{
    public function testValidRequest(): void
    {
        // Test correct retrieval of an existing Summoner.
        $method = new Summoner('DS Examo');
        $collection = $this->apiClient->request($method);
        // Test correct retrieval of a Collection.
        $this->assertInstanceOf(Collection::class, $collection);
        $this->assertInstanceOf(\App\Models\Summoner::class, $collection->first());
        // Expect the collection to be persisted to the Database.
        $this->assertDatabaseHas('summoners', $collection->first()->getAttributes());
        // Expect the Collection to be persisted to the Cache.
        $this->assertTrue(Cache::has($method->getCacheKey()));
        $this->assertInstanceOf(Collection::class, Cache::get($method->getCacheKey()));
    }

    public function testInvalidRequest(): void
    {
        // Test error handling of non-existent Summoner
        $method = new Summoner($this->faker->slug);
        $result = $this->apiClient->request($method);
        $this->assertNull($result);
        // Expect this not to be persisted to the Cache.
        $this->assertFalse(Cache::has($method->getCacheKey()));
    }
}
