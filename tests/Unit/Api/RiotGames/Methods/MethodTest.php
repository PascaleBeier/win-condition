<?php

namespace Tests\Unit\Api\RiotGames\Methods;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Tests\Unit\Api\RiotGames\ApiTest;

abstract class MethodTest extends ApiTest
{
    public $method;

    public $model;

    public $table;

    public $validMethodInput;

    public $invalidMethodInput;

    public function testValidRequest(): void
    {
        // Test correct retrieval of an existing Summoner.
        $method = new $this->method($this->validMethodInput);
        $collection = $this->apiClient->request($method);
        // Test correct retrieval of a Collection.
        $this->assertInstanceOf(Collection::class, $collection);
        $this->assertInstanceOf($this->model, $collection->first());
        // Expect the collection to be persisted to the Database if wanted.
        if ($method->hasDatabaseModel) {
            $this->assertDatabaseHas($this->table, $collection->first()->getAttributes());
        }
        // Expect the Collection to be persisted to the Cache if wanted.
        if ($method->hasCache) {
            $this->assertTrue(Cache::has($method->getCacheKey()));
        }
        $this->assertInstanceOf(Collection::class, Cache::get($method->getCacheKey()));
    }

    public function testInvalidRequest(): void
    {
        if ($this->invalidMethodInput) {
            // Test error handling of non-existent Summoner
            $method = new $this->method($this->invalidMethodInput);
            $result = $this->apiClient->request($method);
            $this->assertNull($result);
            // Expect this not to be persisted to the Cache.
            if ($method->hasCache) {
                $this->assertFalse(Cache::has($method->getCacheKey()));
            }
        } else {
            $this->assertTrue(true);
        }
    }
}
