<?php

namespace Tests\Unit\Api\RiotGames\Methods;

use App\Api\RiotGames\Methods\Summoner;

class SummonerTest extends MethodTest
{
    public $table = 'summoners';

    public $model = \App\Models\Summoner::class;

    public $method = Summoner::class;

    public $validMethodInput = 'DS Examo';

    public $invalidMethodInput = 'nonExistant4asdasdasdasdasdasd';
}
