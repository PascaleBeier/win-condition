<?php

return [
    'uri'     => 'https://cdn.communitydragon.org',
    'version' => '8.12.1',
    'json'    => [
        'toArray' => true,
        'client'  => [
            'options' => ['http_errors' => false],
        ],
        'cache' => [
            'enabled'  => true,
            'duration' => 60,
        ],
    ],
];
