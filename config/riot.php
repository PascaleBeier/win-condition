<?php

return [
    'version' => env('RIOT_GAMES_API_VERSION', '8.12.1'),
    'uri'     => env('RIOT_GAMES_API_HOST', 'https://{host}.api.riotgames.com'),
    'key'     => env('RIOT_GAMES_API_KEY', ''),
    'cache'   => env('RIOT_GAMES_API_DEFAULT_CACHE_DURATION', 30),
    'limits'  => env('RIOT_GAMES_API_APP_RATE_LIMITS', '20:1,100:120'),
];
